# About

The repository is demo of simple host-independant Docker-based development environment.
The framework used is Halogen, `basic` example.

## Start working with Purescript in a few commands

0. Install Docker and docker-compose, clone the repository
1. Run `docker-compose up -d`, then `docker-compose exec purescript bash`
2. Now you have shell in container, install all dependencies - `psc-package build`
3. Start devserver - `pulp server --host 0.0.0.0` (we need 0.0.0.0 cause we are inside docker container)
4. Go to <http://localhost:1337/>, now you should see button from Halogen's `basic` example

## Play with the code

Open `project/src/Button.purs` and change:

```purescript
      label = if state then "Onn" else "Off"
```

to:

```purescript
      label = if state then "Open" else "Not so open"
```

Now reload the page, and you should see the changes.

#### Why this works

`project` directory is mounted to the container, so you can edit code of the app using your favorite editor.

`pulp server` would notice changes in the code and recompile the project, showing errors, if any.

## Troubleshooting

- container uses UID 1000, if your UID is not the same there can be some permission problems
- downloading images can take some time
- make sure you are not using port 1337
- using Docker without `sudo` requires being in `docker` group
