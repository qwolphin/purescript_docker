FROM node:9

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global PROJECT_DIR=/home/node/project

USER node

RUN mkdir $PROJECT_DIR $NPM_CONFIG_PREFIX

RUN npm install -g purescript pulp bower psc-package

ENV PATH=$PATH:$NPM_CONFIG_PREFIX/bin

WORKDIR $PROJECT_DIR
